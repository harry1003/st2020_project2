const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true; 

// 1 - example
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
})

// 2 
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let button = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(button).toBe('Join');
    await browser.close();
})

// 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({path: 'test/screenshots/login.png'});
    await browser.close();
})

// 4
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 1500}); 

    await page.screenshot({path: 'test/screenshots/welcome.png'});
    await browser.close();
})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.keyboard.press('Enter', {delay: 1500}); 

    await page.screenshot({path: 'test/screenshots/error.png'});
    await browser.close();
})

// 6
test('[Content] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();

    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#swal2-title');    
    let title = await page.$eval('#swal2-title', (content) => content.innerHTML);
    expect(title).toBe('Welcome!');
    await browser.close();
})

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');

    await browser.close();
})

// 8
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', 100);
    let roomName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(roomName).toBe('R1');

    await browser.close();
})

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');
    await browser.close();
})

// 10
test('[Behavior] Login 2 users', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });

    const page1 = await browser.newPage();
    await page1.goto(url);
    await page1.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page1.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page1.keyboard.press('Enter', {delay: 100}); 

    const page2 = await browser.newPage();
    await page2.goto(url);
    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 

    await page1.waitForSelector('#users > ol > li:nth-child(2)');    
    await page2.waitForSelector('#users > ol > li:nth-child(2)');  

    let member1 = await page1.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });
    let member2 = await page1.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });
    expect(member1).toBe('John');
    expect(member2).toBe('Mike');

    member1 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });
    member2 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });
    expect(member1).toBe('John');
    expect(member2).toBe('Mike');

    await browser.close();
})

// 11
test('[Content] The "Send" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });

    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#message-form > button');    
    let button = await page.$eval('#message-form > button', (content) => content.innerHTML);
    expect(button).toBe('Send');
    await browser.close();
})

// 12
test('[Behavior] Send a message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });

    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#message-form > input[type=text]');    
    await page.type('#message-form > input[type=text]', 'Hi', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#messages > li:nth-child(2) > div.message__body > p');    
    let message = await page.$eval('#messages > li:nth-child(2) > div.message__body > p', (content) => content.innerHTML);
    expect(message).toBe('Hi');
    await browser.close();
})

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });

    const page1 = await browser.newPage();
    await page1.goto(url);
    await page1.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page1.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page1.keyboard.press('Enter', {delay: 100}); 

    const page2 = await browser.newPage();
    await page2.goto(url);
    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 

    await page1.waitForSelector('#message-form > input[type=text]');    
    await page1.type('#message-form > input[type=text]', 'Hi', {delay: 100});
    await page1.keyboard.press('Enter', {delay: 100}); 

    await page2.waitForSelector('#message-form > input[type=text]');    
    await page2.type('#message-form > input[type=text]', 'Hello', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 

    await page1.waitForSelector('#messages > li:nth-child(4) > div.message__body > p');
    let user11 = await page1.$eval('#messages > li:nth-child(3) > div.message__title > h4', (content) => content.innerHTML);
    let message11 = await page1.$eval('#messages > li:nth-child(3) > div.message__body > p', (content) => content.innerHTML);
    let user12 = await page1.$eval('#messages > li:nth-child(4) > div.message__title > h4', (content) => content.innerHTML);
    let message12 = await page1.$eval('#messages > li:nth-child(4) > div.message__body > p', (content) => content.innerHTML);
    expect(user11).toBe('John');
    expect(message11).toBe('Hi');
    expect(user12).toBe('Mike');
    expect(message12).toBe('Hello');

    await page2.waitForSelector('#messages > li:nth-child(3) > div.message__body > p');
    let user21 = await page2.$eval('#messages > li:nth-child(2) > div.message__title > h4', (content) => content.innerHTML);
    let message21 = await page2.$eval('#messages > li:nth-child(2) > div.message__body > p', (content) => content.innerHTML);
    let user22 = await page2.$eval('#messages > li:nth-child(3) > div.message__title > h4', (content) => content.innerHTML);
    let message22 = await page2.$eval('#messages > li:nth-child(3) > div.message__body > p', (content) => content.innerHTML);
    expect(user21).toBe('John');
    expect(message21).toBe('Hi');
    expect(user22).toBe('Mike');
    expect(message22).toBe('Hello');

    await browser.close();
})

// 14
test('[Content] The "Send location" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });

    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#send-location');    
    let button = await page.$eval('#send-location', (content) => content.innerHTML);
    expect(button).toBe('Send location');
    await browser.close();
})


// 15
test('[Behavior] Send a location message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const context = browser.defaultBrowserContext();
    context.clearPermissionOverrides();
    context.overridePermissions('http://localhost:3000', ['geolocation']);

    const page = await browser.newPage();
    await page.setGeolocation({
        latitude: 51.5074,
        longitude: 0.1278
      });

    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#send-location'); 
    await page.waitFor(2000);
    await page.click('#send-location'); 

    await page.waitForSelector('#messages > li:nth-child(2) > div.message__body');    
    let message = await page.$eval('#messages > li:nth-child(2) > div.message__body', (content) => content.innerHTML);

    await page.waitForSelector('#messages > li:nth-child(2) > div.message__body');    
    let msg = await page.$eval('#messages > li:nth-child(2) > div.message__body > div > iframe ', (content) => content.innerHTML);
    expect(msg).toBe('<a href=\"https://www.maps.ie/coordinates.html\">find my coordinates</a>');
    await browser.close();
})
